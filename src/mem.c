#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }


static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}


static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }


extern inline bool region_is_invalid( const struct region* r );


static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}


/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  // get size from query (capacity)
  block_size b_size = size_from_capacity((block_capacity){.bytes = query});

  // get actual size for region
  size_t size = region_actual_size(b_size.bytes);

  // try alloc fixed region
  void* region_addr = map_pages(addr, size, MAP_FIXED_NOREPLACE);

  // if fail - try not fixed
  if (region_addr == MAP_FAILED) {
      region_addr = map_pages(addr, size, 0);
      // if fail again - :(
      if (region_addr == MAP_FAILED) {
          return REGION_INVALID;
      }
  }

  // check if extends
  bool extends = region_addr == addr;
  
  // create region
  struct region allocated = {.addr = region_addr, .extends = extends, .size = size};

  // init block and return
  b_size = (block_size){.bytes = allocated.size};
  block_init(region_addr, b_size, NULL);
  return allocated;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {

  // check if we can split
  bool splittable = block_splittable(block, query);
  if (!splittable) {
      return false;
  }

  // calc new block addr and size
  void* new_block = block->contents + query;
  block_size new_block_size = (block_size) {.bytes = block->capacity.bytes - query};

  // init new block
  block_init(new_block, new_block_size, block->next);

  // update block->next
  block->next = new_block;

  // update capacity
  block->capacity.bytes = query;
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  // get next
  struct block_header* next = block->next;

  // check if it has next
  if (next == NULL) {
      return false;
  }

  // check if mergeable
  bool can_merge = mergeable(block, next);

  // if not - fail
  if (!can_merge) {
      return false;
  }

  // update capacity
  block_size new = size_from_capacity(next->capacity);
  block->capacity.bytes += new.bytes;

  // update block->next
  block->next = next->next;
  return can_merge;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  struct block_search_result result = {0};

  // check if corrupted
  bool cycle = block->next == block;
  if (cycle) {
      result.block = block;
      result.type = BSR_CORRUPTED;
      return result;
  }
  
  // cycle
  while (block != NULL) {
      // try to merge to get big enough
      if (block->is_free) {
          while (try_merge_with_next(block)) {};
          
          // check if it's enough
          bool is_enough = block_is_big_enough(sz, block);
          if (is_enough) {
              result.block = block;
              result.type = BSR_FOUND_GOOD_BLOCK;
              return result;
          }
      }
      // break cycle if needed
      struct block_header* next = block->next;
      if (next != NULL) {
          block = next;
      } else {
          break;
      }
  }
  
  // return last
  result.block = block;
  result.type = BSR_REACHED_END_NOT_FOUND;
  return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  // try to find
  struct block_search_result result = find_good_or_last(block, query);

  // check success
  bool success = result.type == BSR_FOUND_GOOD_BLOCK;
  if (!success) {
      return result;
  }

  // try to split
  struct block_header* found_block = result.block;
  split_if_too_big(found_block, query);

  // set not free
  found_block->is_free = false;
  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  // get new region addr
  void* new_reg_addr = block_after(last);

  // try to alloc region
  struct region new_reg = alloc_region(new_reg_addr, query);

  // check if fail
  bool invalid = region_is_invalid(&new_reg);
  if (invalid) {
      return NULL;
  }

  // if success - set new address
  last->next = new_reg.addr;

  // finally, try merge
  bool merged = try_merge_with_next(last);
  if (merged) {
      return last;
  } else {
      return new_reg.addr;
  }
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  // get actual size
  if (query < BLOCK_MIN_CAPACITY) {
      query = BLOCK_MIN_CAPACITY;
  }

  // try to alloc
  struct block_search_result result = try_memalloc_existing(query, heap_start);

  // check success
  bool success = result.type == BSR_FOUND_GOOD_BLOCK;
  if (success) {
      return result.block;
  }

  // check if heap is small
  bool small_heap = result.type == BSR_REACHED_END_NOT_FOUND;
  if (small_heap) {
      // get last block
      struct block_header* last = result.block;

      // try to grow heap
      struct block_header* block = grow_heap(last, query);

      // check fail
      if (block == NULL) {
          return block;
      }
      // try to alloc again
      struct block_search_result new_block = try_memalloc_existing(query, block);

      // return result
      return new_block.block;
  }

  // no more ways to alloc
  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;

  // try merge
  while (try_merge_with_next(header)) {};
}
