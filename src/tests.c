#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"

#include <stdbool.h>
#include <stdio.h>

#include "tests.h"

static void destroy_heap(void* heap, size_t heap_size) {
    block_capacity capacity = {.bytes = heap_size};
    block_size size = size_from_capacity(capacity);
    munmap(heap, size.bytes);
}

void test1() {
    printf("Test 1 started\n");
    void* heap = heap_init(4096);
    if (heap == NULL) {
        printf("Heap init failed\n");
        return;
    }
    printf("Heap init success\n");
    debug_heap(stdout, heap);

    void* alloc = _malloc(2048);
    if (alloc == NULL) {
        printf("Allocate failed\n");
        return;
    }
    printf("Allocate success\n");
    debug_heap(stdout, heap);

    _free(alloc);
    printf("Free\n");
    debug_heap(stdout, heap);

    destroy_heap(heap, 4096);
    printf("Test 1 passed\n");
}

void test2() {
    printf("Test 2 started\n");
    void* heap = heap_init(4096);
    if (heap == NULL) {
        printf("Heap init failed\n");
        return;
    }
    printf("Heap init success\n");
    debug_heap(stdout, heap);

    void* alloc1 = _malloc(512);
    void* alloc2 = _malloc(512);
    void* alloc3 = _malloc(512);
    printf("Allocate success\n");
    debug_heap(stdout, heap);

    _free(alloc2);
    printf("Free center\n");
    debug_heap(stdout, heap);

    _free(alloc1);
    _free(alloc3);

    destroy_heap(heap, 4096);
    printf("Test 2 passed\n");
}

void test3() {
    printf("Test 3 started\n");
    void* heap = heap_init(4096);
    if (heap == NULL) {
        printf("Heap init failed\n");
        return;
    }
    printf("Heap init success\n");
    debug_heap(stdout, heap);

    void* alloc1 = _malloc(512);
    void* alloc2 = _malloc(512);
    void* alloc3 = _malloc(512);
    void* alloc4 = _malloc(512);
    void* alloc5 = _malloc(512);
    printf("Allocate success\n");
    debug_heap(stdout, heap);

    _free(alloc4);
    printf("Free 4\n");
    debug_heap(stdout, heap);

    _free(alloc3);
    printf("Free 3\n");
    debug_heap(stdout, heap);

    _free(alloc2);
    printf("Free 2\n");
    debug_heap(stdout, heap);

    _free(alloc5);
    printf("Free 5\n");
    debug_heap(stdout, heap);

    _free(alloc1);

    destroy_heap(heap, 4096);
    printf("Test 3 passed\n");
}

void test4() {
    printf("Test 4 started\n");
        void* heap = heap_init(2048);
        if (heap == NULL) {
            printf("Heap init failed\n");
        return;
    }
    printf("Heap init success\n");
    debug_heap(stdout, heap);

    void* alloc = _malloc(4096);
    if (alloc == NULL) {
        printf("Allocate failed\n");
        return;
    }
    printf("Allocate success\n");
    debug_heap(stdout, heap);

    struct block_header* header = (struct block_header*) heap;
    bool correct = header->capacity.bytes >= 4096;
    if (!correct) {
        printf("Not correct allocation\n");
        return;
    }

    _free(alloc);
    printf("Free\n");
    debug_heap(stdout, heap);

    destroy_heap(heap, 4096);
    printf("Test 4 passed\n");
}

void test5() {
    printf("Test 5 started\n");
    void* heap = heap_init(4096);
    if (heap == NULL) {
        printf("Heap init failed\n");
        return;
    }
    printf("Heap init success\n");
    debug_heap(stdout, heap);

    struct block_header* alloc_header = heap;
    void* wall = mmap(
            alloc_header->contents + alloc_header->capacity.bytes,
            4096,
            PROT_READ | PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS,
            -1,
            0
            );
    printf("Wall allocated after heap\n");

    void* alloc = _malloc(4096 * 2);
    if (alloc == NULL) {
        printf("Allocate failed\n");
        return;
    }
    printf("Allocate success\n");
    debug_heap(stdout, heap);

    struct block_header* header = (struct block_header*) heap;
    bool correct = header->is_free && !header->next->is_free;
    if (!correct) {
        printf("Not correct allocation\n");
        return;
    }

    _free(alloc);
    printf("Free\n");
    debug_heap(stdout, heap);

    munmap(wall, 4096);
    destroy_heap(heap, 4096);
    printf("Test 5 passed\n");
}